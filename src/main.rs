use std::io::BufRead;

/// A parsed Apt package entry.
struct AptEntry<'a> {
	name: &'a str,
	_origins: Vec<&'a str>,
	_version: &'a str,
	_arch: &'a str,
	manually_installed: bool,
}

impl<'a> AptEntry<'a> {
	pub fn new(entry: &'a str) -> Option<Self> {
		// the different parts are space-separated
		let mut parts = entry.split(' ');
		// name and version are separated by a single '/'
		let mut first = parts.next()?.split('/');

		let name = first.next()?;
		let _origins: Vec<&'a str> = first.next()?.split(',').collect();
		let _version = parts.next()?;
		let _arch = parts.next()?;
		let manually_installed = !parts.next()?.contains("automatic");

		Some(AptEntry {
			name,
			_origins,
			_version,
			_arch,
			manually_installed,
		})
	}
}

/// Takes the output of `apt list --installed` and lists the names of all
/// not automatically (manually) installed packages.
fn main() -> std::io::Result<()> {
	let mut result = String::with_capacity(4096);

	for line in std::io::stdin().lock().lines().skip(1) {
		let input = line?;

		let parsed = AptEntry::new(&input);
		if let Some(entry) = parsed {
			if entry.manually_installed {
				result.push_str(&entry.name);
				result.push(' ');
			}
		} else {
			eprintln!("Failed to parse: \"{}\"", &input);
		}
	}

	println!("{}", result);
	Ok(())
}
