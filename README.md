# Apt-parser

Parses the output of `apt list --installed` and lists the names of all manually installed packages.
These can then be used to easily re-install this Debian installation.

Since apt lists many packages as not automatically installed which were actually
automatically installed, the output probably contains some packages you did not
install manually.

Tested with `apt 2.2.2 (amd64)`.

## Install
You first have to install [Rust](https://rustup.rs/) to compile the program.

Then you can:
```terminal
felix@test:~$ git clone https://gitlab.com/SlaynAndKorpil/apt-parser
felix@test:~$ cd apt-parser
felix@test:~/apt-parser$ cargo build --release
```

The resulting binary is then located in `target/release/apt-parser`.

## Example
```terminal
felix@test:~$ apt list --installed | apt-parser
adb adduser amdgpu-pro-pin apt-listchanges apt-utils apt [...] xterm xwallpaper xxd zathura zlib1g zsh
```
The output in this example is shortened since this list can get quite long.

## License

See [LICENSE](https://gitlab.com/SlaynAndKorpil/apt-parser/-/blob/master/LICENSE) or the LICENSE file that should be included in your copy of this software.
